import React, {useState, useRef, useEffect} from 'react';
import Dropzone from 'react-dropzone';
import sha1 from 'sha1'

interface B2UploadUrlReponse {
  authorizationToken: string,
  bucketId: string, 
  uploadUrl: string
}

const uploadFile = (image: File) => {

  fetch('/api/get-upload-url')
    .then(resp => resp.json())
    .then(({ authorizationToken, uploadUrl }: B2UploadUrlReponse) => {

      return fetch(uploadUrl, {
        headers: {
          'Authorization': authorizationToken,
          'X-Bz-File-Name': image.name,
          'X-Bz-Content-Sha1': 'do_not_verify',
          'Content-Type': 'b2/x-auto'
        },
        method: 'POST',
        mode: 'cors',
        body: image
      })

    })
    .then(resp => resp.json())
    .then((resp) => {
      console.log('resp ', resp);
    })
    .catch((e: any) => {
      console.error(e)
    })
}


const Page = () => {


  return (
    <div style={{ padding: "2em" }}>
      <Dropzone onDrop={(acceptedFiles: File[]) => uploadFile(acceptedFiles[0])}>
      {({ getRootProps, getInputProps }) => (
        <section>
          <div {...getRootProps()}>
            <input {...getInputProps()} /> 
            <div style={{ 
              height: "200px",
              width: "300px",
              border: "2px dashed #9c9c9c",
              cursor: "pointer"
            }}>
              
            </div> 
          </div> 
        </section>
      )}
      </Dropzone>
    </div>
  );
}

export default Page