// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import config from '../../config';

type Data = {
  name: string
}

const permissions = [
  "listBuckets",
  "readBuckets",
  "listFiles",
  "readFiles",
  "shareFiles",
  "writeFiles",
  "deleteFiles",
  "readBucketEncryption",
  "writeBucketEncryption",
  "readBucketReplications",
  "writeBucketReplications"
];

interface AuthResponse {
  "absoluteMinimumPartSize": number,
  "accountId": string,
  "allowed": {
    "bucketId": string,
    "bucketName": string,
    "capabilities": Array<typeof permissions[number]>,
    "namePrefix"?: string
  },
  "apiUrl": string,
  "authorizationToken": string,
  "downloadUrl": string,
  "recommendedPartSize": number,
  "s3ApiUrl": string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

  const authStr = Buffer.from(config.keyID + ':' + config.applicationKey).toString('base64');

  fetch('https://api.backblazeb2.com/b2api/v2/b2_authorize_account', {
    headers: {
      'Authorization': `Basic ${authStr}`
    }
  })
  .then(resp => resp.json())
  .then((data: AuthResponse) => {

    if(data?.allowed?.capabilities?.indexOf('writeFiles') != null) {

      return fetch(`${data.apiUrl}/b2api/v2/b2_get_upload_url`, {
        headers: {
          'Authorization': data.authorizationToken,
          'content-type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        body: JSON.stringify({
          bucketId: data.allowed.bucketId
        })
      })


    } else {

      res.status(403);
      Promise.reject();
    }

  })
  .then((res: Response|any) => res.json())
  .then((data: any) => {

      res.status(200).json(data);
  })


}
