
## cli command to run

**We may restrict the origin and some headers in production**

```
./b2-darwin update-bucket --corsRules '[    {"corsRuleName": "downloadFromAnyOriginWithUpload", "allowedOrigins": ["*"], "allowedHeaders": ["*"], "allowedOperations": ["b2_download_file_by_id", "b2_download_file_by_name", "b2_upload_file", "b2_upload_part"], "exposeHeaders": ["authorization", "x-bz-file-name", "x-bz-content-sha1"], "maxAgeSeconds": 3600 }]' <YourBucketId> allPublic
```